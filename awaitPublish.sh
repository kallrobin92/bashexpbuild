#!/bin/bash

regexp="\[exp\] ([A-Za-z]+): Build in progress..."
regexp1="\[exp\] ([A-Za-z]+): Build waiting in queue..."

build_status () {
  local __result=$1
  local __prev
  local __output=$(exp build:status)
  while read -r line; do
    if [ "$line" == "" ]; then
      line=$prev
    fi;
  __prev=$line
  done <<< "$__output"
  eval $__result="'$__prev'"
}

while true; do
  build_status result
  if ! [ "$result" =~ $regexp -a "$result" =~ $regexp ]; then
    echo "$result"
    break;
  fi;
  echo "$result";
  sleep 5;
done;
